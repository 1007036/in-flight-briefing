# Safety Briefing

* In the event of FIRE, FAILURE or ABNORMALITY prior to rotate I WILL 

  * CLOSE the throttle 

  * Apply maximum braking

  * Exit the runway at the closest taxiway 

* In the event of FIRE, FAILURE or ABNORMALITY after rotate with remaining
  runway I WILL

  * Lower the nose

  * CLOSE the throttle

  * Land the aircraft on the remaining runway 

  * Apply maximum braking 

  * Exit the runway at the closest taxiway

* In the event of FIRE, FAILURE or ABNORMALITY after rotate WITHOUT remaining
  runway I WILL

  * Lower the nose to adopt the glide attitude

  * Select a landing area within 30 degrees either side of the nose 

  * The first time I will attempt a turn back to the runway will be only if 

    * I have commenced a turn.

    OR

    * Or have sufficient height to glide back to the runway.  
