# Departure Briefing

* Today I/we will be departing to the ...
* Today I/we will be using runway ... which will require a ... hand circuit
  direction.
* Today I will be departing the circuit on the ... leg of the circuit climbing
  to a height of ... [with a heading of ...]
